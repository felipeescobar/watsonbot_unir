package com.dynamicdroides.bots;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SimpleQuestion {

	@JsonProperty("lang")
	private String lang;
	@JsonProperty("context")
	private String context;
	@JsonProperty("question")
	private String question;
	
	public SimpleQuestion(){
		
		
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
	
}
