package com.dynamicdroides.bots;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dynamicdroides.bots.aipai.beans.WatsonResponse;
import com.dynamicdroides.bots.tools.SendGridMailSender;
import com.ibm.watson.developer_cloud.assistant.v1.Assistant;
import com.ibm.watson.developer_cloud.assistant.v1.model.InputData;
import com.ibm.watson.developer_cloud.assistant.v1.model.MessageOptions;
import com.ibm.watson.developer_cloud.assistant.v1.model.RuntimeEntity;

@Service
public class WatsonBot{

	private static final String WATSON_VERSION = "2018-02-16";
	private static final String WATSON_WORKSPACE = "2ac2453f-bb4a-4e7c-ae4a-6701bb2def0d";
	private static final String WATSON_PASSWORD = "HkXv5ge0rlVu";
	private static final String WATSON_USERNAME = "effc99d6-b87f-4c94-b1e1-8e0ced68d274";
	Assistant service;
	private static final String workspaceId = WATSON_WORKSPACE;
	private String emailTo;
	
	
	public WatsonBot(){	
		service = new Assistant(WATSON_VERSION);
		service.setUsernameAndPassword(WATSON_USERNAME, WATSON_PASSWORD);
		emailTo="felipe.escobar@gmail.com";
	}
	/**
	 * TODO: meter un sistema de cache, y una comprobacion de que si esta cargada la respuesta en un bbdd (no sql)
	 * 
	 * @param question
	 * @return
	 */
	public WatsonResponse getAnswer(String question) {
		
		InputData input = new InputData.Builder(question).build();
		
		MessageOptions options = new MessageOptions.Builder(workspaceId)
		  .input(input)
		  .build();
		com.ibm.watson.developer_cloud.assistant.v1.model.MessageResponse response = service.message(options).execute();
		//Esta linea de codigo es un ejemplo del uso del servicio de interpretacion de texto. Mas alla de la respuesta que podamos generar, podemos extraer informacion de los elementos de la frase. Por ejemplo, me gusta mucho Elvis Presley.
		
	
		readEntities(response);
		WatsonResponse responseW=new WatsonResponse();
		
		if(response.getOutput()!=null && !response.getOutput().getText().isEmpty()){
			StringBuffer buf=new StringBuffer();
			for(String s:response.getOutput().getText()){
				buf.append(""+s+"  ");
			}
			responseW.setAnswer(buf.toString());
		}else{
			//TODO tomar del contexto los datos del usuario(en algun punto del dialogo). 
			//TODO de momento funciona el envio de correo 
			SendGridMailSender.send(question,emailTo);
			responseW.setAnswer("No entiendo");			
		}
		if("No entiendo".equals(responseW.getAnswer())){
			SendGridMailSender.send(question,emailTo);
		}
		return responseW;	
	}
	
	private void readEntities(com.ibm.watson.developer_cloud.assistant.v1.model.MessageResponse response){
		System.out.println(response);

		List<com.ibm.watson.developer_cloud.assistant.v1.model.RuntimeEntity>  entidades=(List<com.ibm.watson.developer_cloud.assistant.v1.model.RuntimeEntity>)response.get("entities");
		if(entidades!=null && entidades.size()>0){
	
			System.out.println(entidades.get(0));
			//TODO meter un clase que lea el json/framework
			
		}
		
		
	}
}
