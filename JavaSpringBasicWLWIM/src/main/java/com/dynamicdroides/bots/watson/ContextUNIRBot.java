package com.dynamicdroides.bots.watson;

import java.util.HashMap;

import com.ibm.watson.developer_cloud.assistant.v1.model.Context;

/**
 * Este objeto recoje las distintas entidades
 * @author admin
 *
 */
public class ContextUNIRBot {

	public ContextUNIRBot(){
		
	}
	public HashMap<String,String> getMapaContexto(Context contexto,HashMap<String,String> mapaAux){
		HashMap<String,String> mapa;
		if(mapaAux!=null){
			mapa=mapaAux;
		}else{
			mapa=new HashMap<String,String>();
			
		}
		if(contexto.get("CAMPUS_VIRTUAL")!=null){
			mapa.put("CAMPUS_VIRTUAL",contexto.get("CAMPUS_VIRTUAL").toString());
		}else if(contexto.get("contact_info_email")!=null){
			mapa.put("CONTACT_INFO_EMAIL",contexto.get("CONTACT_INFO_EMAIL").toString());
			
		}else if(contexto.get("contact_info_phone")!=null){
			mapa.put("CONTACT_INFO_PHONE",contexto.get("CONTACT_INFO_PHONE").toString());
			
		}
		return mapa;
		
	}
}
