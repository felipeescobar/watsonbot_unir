
package com.dynamicdroides.bots.aipai.beans;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;


public class WatsonResponse implements Serializable {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 4156648426475754224L;
	
	
	@SerializedName("lang")
	private String lang;
	 @SerializedName("answer")
	private String answer;
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	



   
}
