package com.dynamicdroides.bots;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dynamicdroides.bots.WatsonBot;
import com.dynamicdroides.bots.aipai.beans.WatsonResponse;

 
@RestController
@RequestMapping("/watson")
public class WatsonBotController {
	private final static Logger logger = LoggerFactory.getLogger(WatsonBotController.class);
 
	private WatsonBot bot;
	
	public WatsonBotController(WatsonBot bot){
		this.bot=bot;
	}
	
	
	@RequestMapping(value="/question/simple/", method = RequestMethod.POST,  consumes = MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public WatsonResponse getSimpleAnswer(@RequestBody  SimpleQuestion request){
		logger.debug(request.getQuestion());
		WatsonResponse answer= bot.getAnswer(request.getQuestion());

		return answer;
	}
	
}
